#!/usr/local/bin/python3

from itertools import permutations

class Computer:
    def __init__(self, inp, phase = None, initial_value = None):
        self.inp = inp.copy()
        self.pointer = 0

        self.input_nb = 0
        self.phase = phase
        self.initial_value = initial_value
        self.ouput = None

        self.op_codes = {
            1: {'args': 3, 'command': self.op1},
            2: {'args': 3, 'command': self.op2},
            3: {'args': 1, 'command': self.op3},
            4: {'args': 1, 'command': self.op4},
            5: {'args': 2, 'command': self.op5},
            6: {'args': 2, 'command': self.op6},
            7: {'args': 3, 'command': self.op7},
            8: {'args': 3, 'command': self.op8},
        }

    def resolve_arg(self, arg):
        if arg[1] == '0':
            return self.inp[arg[0]]
        return arg[0]

    # Add
    def op1(self, arg1, arg2, arg3):
        self.inp[arg3[0]] = self.resolve_arg(arg1) + self.resolve_arg(arg2)
        self.pointer += 4

    # Factor
    def op2(self, arg1, arg2, arg3):
        self.inp[arg3[0]] = self.resolve_arg(arg1) * self.resolve_arg(arg2)
        self.pointer += 4

    # Input
    def op3(self, arg1):
        if self.phase != None and self.input_nb == 0:
            self.inp[arg1[0]] = self.phase
        elif self.initial_value != None and self.input_nb == 1:
            self.inp[arg1[0]] = self.initial_value
        else:
            self.inp[arg1[0]] = int(input("Input a value : "))
        self.input_nb += 1
        self.pointer += 2

    # Output
    def op4(self, arg1):
        # print(f"Diagnostic code : {self.resolve_arg(arg1)}")
        self.output = self.resolve_arg(arg1)
        self.pointer += 2

    # Jump if true
    def op5(self, arg1, arg2):
        if self.resolve_arg(arg1):
            self.pointer = self.resolve_arg(arg2)
        else:
            self.pointer += 3

    # Jump if false
    def op6(self, arg1, arg2):
        if not self.resolve_arg(arg1):
            self.pointer = self.resolve_arg(arg2)
        else:
            self.pointer += 3

    # Less than
    def op7(self, arg1, arg2, arg3):
        self.inp[arg3[0]] = int(self.resolve_arg(arg1) < self.resolve_arg(arg2))
        self.pointer += 4

    # Equals
    def op8(self, arg1, arg2, arg3):
        self.inp[arg3[0]] = int(self.resolve_arg(arg1) == self.resolve_arg(arg2))
        self.pointer += 4

    def run(self):
        #print("### STARTING ###")
        while self.pointer < len(self.inp):
            s = str(self.inp[self.pointer])
            op_code = int(s[-2:])

            if op_code == 99:
                break

            param_modes = s[:len(s)-2].zfill(3)[::-1]

            instruction = self.op_codes[op_code]
            nb_args = instruction['args']
            command = instruction['command']

            args = [(self.inp[self.pointer + 1 + arg], param_modes[arg]) for arg in range(nb_args)]
            command(*args)
        return self.output
        #print("### DONE ###")



if __name__ == '__main__':
    with open('input.txt') as fd:
        inp = [int(n) for n in fd.read().strip().split(',')]

    phases = permutations([0, 1, 2, 3, 4])

    outputs = []
    for phase in phases:
        computer = Computer(inp, phase = phase[0], initial_value = 0)
        computer2 = Computer(inp, phase = phase[1], initial_value = computer.run())
        computer3 = Computer(inp, phase = phase[2], initial_value = computer2.run())
        computer4 = Computer(inp, phase = phase[3], initial_value = computer3.run())
        computer5 = Computer(inp, phase = phase[4], initial_value = computer4.run())

        outputs.append((phase, computer5.run()))

    print(max(outputs, key = lambda item: item[1]))

