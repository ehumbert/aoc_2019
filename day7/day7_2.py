#!/usr/local/bin/python3

from itertools import permutations
import asyncio

class Computer:
    def __init__(self, inp, name = None, phase = None, inp_queue = None, out_queue = None):
        self.name = name
        self.inp = inp.copy()
        self.pointer = 0

        self.input_nb = 0
        self.phase = phase
        self.inp_queue = inp_queue
        self.out_queue = out_queue

        self.op_codes = {
            1: {'args': 3, 'command': self.op1},
            2: {'args': 3, 'command': self.op2},
            3: {'args': 1, 'command': self.op3},
            4: {'args': 1, 'command': self.op4},
            5: {'args': 2, 'command': self.op5},
            6: {'args': 2, 'command': self.op6},
            7: {'args': 3, 'command': self.op7},
            8: {'args': 3, 'command': self.op8},
        }

    def resolve_arg(self, arg):
        if arg[1] == '0':
            return self.inp[arg[0]]
        return arg[0]

    # Add
    async def op1(self, arg1, arg2, arg3):
        self.inp[arg3[0]] = self.resolve_arg(arg1) + self.resolve_arg(arg2)
        self.pointer += 4

    # Factor
    async def op2(self, arg1, arg2, arg3):
        self.inp[arg3[0]] = self.resolve_arg(arg1) * self.resolve_arg(arg2)
        self.pointer += 4

    # Input
    async def op3(self, arg1):
        if self.phase != None and self.input_nb == 0:
            self.inp[arg1[0]] = self.phase
        elif self.inp_queue != None and self.input_nb >= 1:
            self.inp[arg1[0]] = await self.inp_queue.get()
        else:
            self.inp[arg1[0]] = int(input("Input a value : "))
        self.input_nb += 1
        self.pointer += 2

    # Output
    async def op4(self, arg1):
        await self.out_queue.put(self.resolve_arg(arg1))
        self.pointer += 2

    # Jump if true
    async def op5(self, arg1, arg2):
        if self.resolve_arg(arg1):
            self.pointer = self.resolve_arg(arg2)
        else:
            self.pointer += 3

    # Jump if false
    async def op6(self, arg1, arg2):
        if not self.resolve_arg(arg1):
            self.pointer = self.resolve_arg(arg2)
        else:
            self.pointer += 3

    # Less than
    async def op7(self, arg1, arg2, arg3):
        self.inp[arg3[0]] = int(self.resolve_arg(arg1) < self.resolve_arg(arg2))
        self.pointer += 4

    # Equals
    async def op8(self, arg1, arg2, arg3):
        self.inp[arg3[0]] = int(self.resolve_arg(arg1) == self.resolve_arg(arg2))
        self.pointer += 4

    async def run(self):
        while self.pointer < len(self.inp):
            s = str(self.inp[self.pointer])
            op_code = int(s[-2:])

            if op_code == 99:
                return

            param_modes = s[:len(s)-2].zfill(3)[::-1]

            instruction = self.op_codes[op_code]
            nb_args = instruction['args']
            command = instruction['command']

            args = [(self.inp[self.pointer + 1 + arg], param_modes[arg]) for arg in range(nb_args)]
            await command(*args)


if __name__ == '__main__':
    with open('input.txt') as fd:
        inp = [int(n) for n in fd.read().strip().split(',')]

    phases = permutations([5, 6, 7, 8, 9])

    outputs = []
    for phase in phases:
        asyncio.set_event_loop(asyncio.new_event_loop())
        loop = asyncio.get_event_loop()

        queue_size = 10
        queue1 = asyncio.Queue(maxsize = queue_size, loop = loop)
        queue2 = asyncio.Queue(maxsize = queue_size, loop = loop)
        queue3 = asyncio.Queue(maxsize = queue_size, loop = loop)
        queue4 = asyncio.Queue(maxsize = queue_size, loop = loop)
        queue5 = asyncio.Queue(maxsize = queue_size, loop = loop)

        # Init value
        queue5.put_nowait(0)

        computer1 = Computer(inp, name = "CPU_1", phase = phase[0], inp_queue=queue5, out_queue=queue1)
        computer2 = Computer(inp, name = "CPU_2", phase = phase[1], inp_queue=queue1, out_queue=queue2)
        computer3 = Computer(inp, name = "CPU_3", phase = phase[2], inp_queue=queue2, out_queue=queue3)
        computer4 = Computer(inp, name = "CPU_4", phase = phase[3], inp_queue=queue3, out_queue=queue4)
        computer5 = Computer(inp, name = "CPU_5", phase = phase[4], inp_queue=queue4, out_queue=queue5)
        
        loop.run_until_complete(asyncio.gather(
            computer1.run(),
            computer2.run(),
            computer3.run(),
            computer4.run(),
            computer5.run()
        ))
        loop.close()

        outputs.append((phase, queue5.get_nowait()))

    best = max(outputs, key = lambda item: item[1])
    print(f"Part 2 : max thrusters signal = {best[1]} with sequence = {best[0]}")
