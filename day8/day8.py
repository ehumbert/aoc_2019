with open('input.txt') as fd:
    image = fd.read().strip()

height = 6
width = 25
image_size = width * height

layers = [image[layer * image_size: (layer + 1) * image_size]
          for layer in range(len(image) // image_size)]

checksum = min(layers, key=lambda layer: layer.count('0'))
print(f"Part 1 : {checksum.count('1') * checksum.count('2')}")

result = []
for pixel in zip(*layers):
    for layer in pixel:
        if layer != '2':
            result.append(layer)
            break

print("Part 2 : ")
for h in range(0, len(result), len(result) // height):
    print(''.join('#' if digit == '1' else ' '
                  for digit in result[h:h + len(result) // height]))
