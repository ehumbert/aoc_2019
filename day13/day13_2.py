#!/usr/local/bin/python3

from itertools import permutations
from os import system, name
from time import sleep
import sys


class Computer:
    def __init__(self, inp, visual=None):
        self.inp = inp.copy()
        self.inp[0] = 2
        self.pointer = 0
        self.relative_base = 0

        self.visual = visual

        self.out = []
        self.score = 0
        self.ball_x = None
        self.paddle_x = None

        self.tiles = {}
        self.tiles_type = {
            0: ' ',
            1: 'X',
            2: '#',
            3: '_',
            4: 'o'
        }

        self.op_codes = {
            1: {'args': 3, 'command': self.op1},
            2: {'args': 3, 'command': self.op2},
            3: {'args': 1, 'command': self.op3},
            4: {'args': 1, 'command': self.op4},
            5: {'args': 2, 'command': self.op5},
            6: {'args': 2, 'command': self.op6},
            7: {'args': 3, 'command': self.op7},
            8: {'args': 3, 'command': self.op8},
            9: {'args': 1, 'command': self.op9}
        }

    def update_tiles(self):
        for i in range(0, len(self.out), 3):
            x, y, tile = self.out[i:i+3]
            if x != -1 or y != 0:
                self.tiles[(x, y)] = tile
                if tile == 4:
                    self.ball_x = x
                elif tile == 3:
                    self.paddle_x = x
            else:
                self.score = tile
        self.out.clear()

    def display_tiles(self):
        print(f"Score : {self.score}")
        for y in range(20):
            s = ''
            for x in range(38):
                s += self.tiles_type.get(self.tiles.get((x, y), None), ' ')
            print(s)

    def clear(self):
        if name == 'nt':
            system('cls')
        else:
            system('clear')

    def resolve_pos(self, arg):
        if arg[1] == '0':
            return arg[0]
        return self.relative_base + arg[0]

    def resolve_arg(self, arg):
        if arg[1] == '0' or arg[1] == '2':
            return self.inp.get(self.resolve_pos(arg), 0)
        return arg[0]

    # Add
    def op1(self, arg1, arg2, arg3):
        self.inp[self.resolve_pos(arg3)] = self.resolve_arg(
            arg1) + self.resolve_arg(arg2)
        self.pointer += 4

    # Factor
    def op2(self, arg1, arg2, arg3):
        self.inp[self.resolve_pos(arg3)] = self.resolve_arg(
            arg1) * self.resolve_arg(arg2)
        self.pointer += 4

    # Input
    def op3(self, arg1):
        self.update_tiles()
        if self.visual:
            sleep(0.02)
            self.clear()
            self.display_tiles()

        if self.paddle_x < self.ball_x:
            move = 1
        elif self.paddle_x > self.ball_x:
            move = -1
        else:
            move = 0

        self.inp[self.resolve_pos(arg1)] = move
        self.pointer += 2

    # Output
    def op4(self, arg1):
        self.out.append(self.resolve_arg(arg1))
        self.pointer += 2

    # Jump if true
    def op5(self, arg1, arg2):
        if self.resolve_arg(arg1):
            self.pointer = self.resolve_arg(arg2)
        else:
            self.pointer += 3

    # Jump if false
    def op6(self, arg1, arg2):
        if not self.resolve_arg(arg1):
            self.pointer = self.resolve_arg(arg2)
        else:
            self.pointer += 3

    # Less than
    def op7(self, arg1, arg2, arg3):
        self.inp[self.resolve_pos(arg3)] = int(
            self.resolve_arg(arg1) < self.resolve_arg(arg2))
        self.pointer += 4

    # Equals
    def op8(self, arg1, arg2, arg3):
        self.inp[self.resolve_pos(arg3)] = int(
            self.resolve_arg(arg1) == self.resolve_arg(arg2))
        self.pointer += 4

    # Modify relative base
    def op9(self, arg1):
        self.relative_base += self.resolve_arg(arg1)
        self.pointer += 2

    def run(self):
        while self.pointer < len(self.inp):
            s = str(self.inp[self.pointer])
            op_code = int(s[-2:])

            if op_code == 99:
                self.update_tiles()
                if self.visual:
                    self.clear()
                    self.display_tiles()
                else:
                    print(f"Score : {self.score}")
                return

            param_modes = s[:len(s)-2].zfill(3)[::-1]

            instruction = self.op_codes[op_code]
            nb_args = instruction['args']
            command = instruction['command']

            args = [(self.inp[self.pointer + 1 + arg], param_modes[arg])
                    for arg in range(nb_args)]
            command(*args)


if __name__ == '__main__':
    with open('input.txt') as fd:
        inp = {i: int(op)
               for (i, op) in enumerate(fd.read().strip().split(','))}

    # Remove second parameter to disable the visual
    computer = Computer(inp, True)
    computer.run()
