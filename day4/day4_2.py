from itertools import groupby

count = 0
for pwd in range(245182, 790572):
    s = str(pwd)
    same_digit = False
    growing = True

    for i in range(len(s) - 1):
        if s[i] > s[i + 1]:
            growing = False
            break

    for(k, g) in groupby(s):
        if len(list(g)) == 2:
            same_digit = True
            break

    if same_digit and growing:
        count += 1

print(count)
