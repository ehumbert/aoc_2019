count = 0
for pwd in range(245182, 790572):
    # for pwd in range(111121, 111123):
    s = str(pwd)
    same_digit = False
    growing = True

    for i in range(len(s) - 1):
        same_digit |= s[i] == s[i + 1]
        if s[i] > s[i + 1]:
            growing = False
            break

    if same_digit and growing:
        count += 1

print(count)
