with open('input.txt') as fd:
    inp = [int(n) for n in fd.read().strip().split(',')]

for noun in range(100):
    for verb in range(100):
        inps = inp.copy()

        inps[1] = noun
        inps[2] = verb

        for i in range(0, len(inps), 4):
            if inps[i] == 1:
                inps[inps[i+3]] = inps[inps[i+1]] + inps[inps[i+2]]
            elif inps[i] == 2:
                inps[inps[i+3]] = inps[inps[i+1]] * inps[inps[i+2]]
            elif inps[i] == 99:
                break

        if noun == 12 and verb == 2:
            print(f"Part 1 : {inps[0]}")

        if inps[0] == 19690720:
            print(f"Part 2 : {100 * noun + verb}")
