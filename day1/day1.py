fuels = [(int(line.strip()) // 3) - 2 for line in open('input.txt')]
total_fuel = 0
for fuel in fuels:
    next_fuel = fuel
    while (next_fuel:= (next_fuel // 3) - 2) > 0:
        total_fuel += next_fuel
    total_fuel += fuel
print(f"Part 1 : {sum(fuels)}")
print(f"Part 2 : {total_fuel}")
