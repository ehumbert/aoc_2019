from math import atan2, pi
from collections import defaultdict


def get_angle(start, destination):
    return atan2(destination[0] - start[0], start[1] - destination[1]) % (2 * pi)


def get_distance(start, destination):
    return abs(destination[0] - start[0]) + abs(destination[1] - start[1])


asteroids = []
for (y, line) in enumerate(open('input.txt')):
    for (x, pos) in enumerate(line.strip()):
        if pos == '#':
            asteroids.append((x, y))

results = []
# Group angles in set (ignoring repeated angles and thus hidden asteroids)
for asteroid in asteroids:
    results.append((asteroid, {get_angle(asteroid, other)
                               for other in asteroids if asteroid != other}))

station = max(results, key=lambda item: len(item[1]))
print(f"Part 1 : {len(station[1])}")

# Group by angle
visibles = defaultdict(list)
for asteroid in asteroids:
    if asteroid == station[0]:
        continue
    visibles[get_angle(station[0], asteroid)].append(asteroid)

# Sort by angle & remove accordingly
count = 0
for angle in sorted(visibles.keys()):
    count += 1
    closest = min(visibles[angle],
                  key=lambda coord: get_distance(coord, station[0]))
    if count == 200:
        print(f"Part 2 : {100 * closest[0] + closest[1]}")
        break
    visibles[angle].remove(closest)
