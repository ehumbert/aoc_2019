#!/usr/local/bin/python3

import re
from functools import reduce
from math import gcd

class Planet:
    def __init__(self, x, y, z):
        self.x = int(x)
        self.y = int(y)
        self.z = int(z)

        self.vx = 0
        self.vy = 0
        self.vz = 0

    def update_velocity(self, planet):
        if self.x < planet.x:
            self.vx += 1
        elif self.x > planet.x:
            self.vx -= 1

        if self.y < planet.y:
            self.vy += 1
        elif self.y > planet.y:
            self.vy -= 1

        if self.z < planet.z:
            self.vz += 1
        elif self.z > planet.z:
            self.vz -= 1
    
    def update_pos(self):
        self.x += self.vx
        self.y += self.vy
        self.z += self.vz

    def get_energy(self):
        return sum(map(abs, [self.x, self.y, self.z])) * sum(map(abs, [self.vx, self.vy, self.vz]))

    def __repr__(self):
        return f"pos<x = {self.x}, y = {self.y}, z = {self.z}> vel<x = {self.vx}, y = {self.vy}, z = {self.vz}>"

planets = []
for (i, line) in enumerate(open("input.txt")):
    planets.append(Planet(*re.findall(r"-?\d+", line.strip())))

for i in range(1000):
    planets[0].update_velocity(planets[1])
    planets[0].update_velocity(planets[2])
    planets[0].update_velocity(planets[3])

    planets[1].update_velocity(planets[0])
    planets[1].update_velocity(planets[2])
    planets[1].update_velocity(planets[3])

    planets[2].update_velocity(planets[0])
    planets[2].update_velocity(planets[1])
    planets[2].update_velocity(planets[3])

    planets[3].update_velocity(planets[0])
    planets[3].update_velocity(planets[1])
    planets[3].update_velocity(planets[2])

    planets[0].update_pos()
    planets[1].update_pos()
    planets[2].update_pos()
    planets[3].update_pos()

print(f"Part 1 : {sum(map(lambda planet: planet.get_energy(), planets))}")