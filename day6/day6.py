def count_orbits(destination):
    if (next_orbit: = reverse_orbit.get(destination, False)):
        return 1 + count_orbits(next_orbit)
    return 0


def get_path(destination, path, start=None):
    if (next_orbit: = reverse_orbit.get(destination, False)) and next_orbit != start:
        return get_path(next_orbit, path, start) + [destination]
    return [destination]


reverse_orbit = {}

for line in open('input.txt'):
    origin, move = line.strip().split(')')
    reverse_orbit[move] = origin

orbits = 0
for leaf in reverse_orbit.keys():
    orbits += count_orbits(leaf)

print(f"Part 1 : {orbits}")

YOU_path = get_path('YOU', [])
SAN_path = get_path('SAN', [])

# Last common ancestor
for (i, obj) in enumerate(YOU_path):
    if obj not in SAN_path:
        last_ancestor = SAN_path[i - 1]
        break

print(f"Part 2 : {len(get_path('YOU', [], last_ancestor)) + len(get_path('SAN', [], last_ancestor)) - 2}")
