from collections import namedtuple, defaultdict
from math import ceil
import re


Component = namedtuple('Component', 'quantity name')

reactions = {}
wastes = defaultdict(int)
for line in open("input.txt"):
    left, right = line.strip().split('=>')

    result = Component(*right.split())
    components = [Component(*component) for component in re.findall(r"(\d+)\s(\w+)", left)]
    reactions[result.name] = {'quantity': result.quantity, 'components': components}

def craft(name, quantity_asked):
    if name == 'ORE':
        return quantity_asked

    reaction = reactions[name]
    quantity_produced = int(reaction['quantity'])
    components = reaction['components']

    stock = wastes.get(name, 0)

    # Everything in stock, no need to craft anything
    if stock >= quantity_asked:
        wastes[name] -= quantity_asked
        return 0

    # If there's any leftover, use it
    quantity_asked -= stock
    wastes[name] = 0

    batches = ceil(quantity_asked / quantity_produced)
    waste = (batches * quantity_produced) - quantity_asked
    wastes[name] += waste

    ores = 0
    for component in components:
        quantity_needed = batches * int(component.quantity)
        ores += craft(component.name, quantity_needed)

    return ores


if __name__ == '__main__':
    print(f"Part 1 : {craft('FUEL', 1)}")

    # Binary search
    low = 0
    mid = None
    high = 10**12

    target = 1000000000000

    while (next_mid := low + ((high - low) // 2)) != mid:
        mid = next_mid
        ores = craft('FUEL', mid)

        if ores < target:
            low = mid
        else:
            high = mid

    print(f"Part 2 : {mid}")
