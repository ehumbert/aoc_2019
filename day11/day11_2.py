#!/usr/local/bin/python3

from itertools import permutations
import operator


class Computer:
    def __init__(self, inp):
        self.inp = inp.copy()
        self.pointer = 0
        self.relative_base = 0

        self.pos = (0, 0)
        self.min_x = 999
        self.max_x = 0
        self.min_y = 999
        self.max_y = 0

        self.direction = (0, 1)
        self.panels = {(0, 0): 1}
        self.out = 0

        self.op_codes = {
            1: {'args': 3, 'command': self.op1},
            2: {'args': 3, 'command': self.op2},
            3: {'args': 1, 'command': self.op3},
            4: {'args': 1, 'command': self.op4},
            5: {'args': 2, 'command': self.op5},
            6: {'args': 2, 'command': self.op6},
            7: {'args': 3, 'command': self.op7},
            8: {'args': 3, 'command': self.op8},
            9: {'args': 1, 'command': self.op9}
        }

    def rotate(self, direction):
        self.direction = (-1 * self.direction[1], self.direction[0]) if direction == 0 else (
            self.direction[1], -1 * self.direction[0])

    def resolve_pos(self, arg):
        if arg[1] == '0':
            return arg[0]
        return self.relative_base + arg[0]

    def resolve_arg(self, arg):
        if arg[1] == '0' or arg[1] == '2':
            return self.inp.get(self.resolve_pos(arg), 0)
        return arg[0]

    # Add
    def op1(self, arg1, arg2, arg3):
        self.inp[self.resolve_pos(arg3)] = self.resolve_arg(
            arg1) + self.resolve_arg(arg2)
        self.pointer += 4

    # Factor
    def op2(self, arg1, arg2, arg3):
        self.inp[self.resolve_pos(arg3)] = self.resolve_arg(
            arg1) * self.resolve_arg(arg2)
        self.pointer += 4

    # Input
    def op3(self, arg1):
        # print(f"Input : {self.panels.get(self.pos, 0)}")
        self.inp[self.resolve_pos(arg1)] = self.panels.get(self.pos, 0)
        self.pointer += 2

    # Output
    def op4(self, arg1):
        if self.out % 2 == 0:
            self.panels[self.pos] = self.resolve_arg(arg1)
        else:
            self.rotate(self.resolve_arg(arg1))
            self.pos = tuple(map(operator.add, self.pos, self.direction))

            self.min_x = min(self.min_x, self.pos[0])
            self.min_y = min(self.min_y, self.pos[1])
            self.max_x = max(self.max_x, self.pos[0])
            self.max_y = max(self.max_y, self.pos[1])
        self.out += 1
        self.pointer += 2

    # Jump if true
    def op5(self, arg1, arg2):
        if self.resolve_arg(arg1):
            self.pointer = self.resolve_arg(arg2)
        else:
            self.pointer += 3

    # Jump if false
    def op6(self, arg1, arg2):
        if not self.resolve_arg(arg1):
            self.pointer = self.resolve_arg(arg2)
        else:
            self.pointer += 3

    # Less than
    def op7(self, arg1, arg2, arg3):
        self.inp[self.resolve_pos(arg3)] = int(
            self.resolve_arg(arg1) < self.resolve_arg(arg2))
        self.pointer += 4

    # Equals
    def op8(self, arg1, arg2, arg3):
        self.inp[self.resolve_pos(arg3)] = int(
            self.resolve_arg(arg1) == self.resolve_arg(arg2))
        self.pointer += 4

    # Modify relative base
    def op9(self, arg1):
        self.relative_base += self.resolve_arg(arg1)
        self.pointer += 2

    def run(self):
        while self.pointer < len(self.inp):
            s = str(self.inp[self.pointer])
            op_code = int(s[-2:])

            if op_code == 99:
                # Printing upside down
                for y in range(self.min_y, self.max_y + 1):
                    s = ''
                    for x in range(self.min_x, self.max_x + 1):
                        s += '#' if self.panels.get((x, y), 0) else '.'
                    print(s)
                return

            param_modes = s[:len(s)-2].zfill(3)[::-1]

            instruction = self.op_codes[op_code]
            nb_args = instruction['args']
            command = instruction['command']

            args = [(self.inp[self.pointer + 1 + arg], param_modes[arg])
                    for arg in range(nb_args)]
            command(*args)


if __name__ == '__main__':
    with open('input.txt') as fd:
        inp = {i: int(op)
               for (i, op) in enumerate(fd.read().strip().split(','))}

    computer = Computer(inp)
    computer.run()
