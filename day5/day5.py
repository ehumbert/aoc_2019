#!/usr/local/bin/python3

with open('input.txt') as fd:
    inp = [int(n) for n in fd.read().strip().split(',')]

def resolve_arg(arg):
    if arg[1] == '0':
        return inp[arg[0]]
    return arg[0]

# Add
def op1(arg1, arg2, arg3, pointer = None):
    inp[arg3[0]] = resolve_arg(arg1) + resolve_arg(arg2)
    return pointer + 4

# Factor
def op2(arg1, arg2, arg3, pointer = None):
    inp[arg3[0]] = resolve_arg(arg1) * resolve_arg(arg2)
    return pointer + 4

# Input
def op3(arg1, pointer = None):
    inp[arg1[0]] = int(input("Input a value : "))
    return pointer + 2

# Output
def op4(arg1, pointer = None):
    print(f"Diagnostic code : {resolve_arg(arg1)}")
    return pointer + 2

# Jump if true
def op5(arg1, arg2, pointer = None):
    if resolve_arg(arg1):
        return resolve_arg(arg2)
    return pointer + 3

# Jump if false
def op6(arg1, arg2, pointer = None):
    if not resolve_arg(arg1):
        return resolve_arg(arg2)
    return pointer + 3

# Less than
def op7(arg1, arg2, arg3, pointer = None):
    inp[arg3[0]] = int(resolve_arg(arg1) < resolve_arg(arg2))
    return pointer + 4

# Equals
def op8(arg1, arg2, arg3, pointer = None):
    inp[arg3[0]] = int(resolve_arg(arg1) == resolve_arg(arg2))
    return pointer + 4

op_codes = {
    1: {'args': 3, 'command': op1},
    2: {'args': 3, 'command': op2},
    3: {'args': 1, 'command': op3},
    4: {'args': 1, 'command': op4},
    5: {'args': 2, 'command': op5},
    6: {'args': 2, 'command': op6},
    7: {'args': 3, 'command': op7},
    8: {'args': 3, 'command': op8},
}

i = 0
while i < len(inp):
    s = str(inp[i])
    op_code = int(s[-2:])

    if op_code == 99:
        break

    param_modes = s[:len(s)-2].zfill(3)[::-1]

    instruction = op_codes[op_code]
    nb_args = instruction['args']
    command = instruction['command']

    args = [(inp[i + 1 + arg], param_modes[arg]) for arg in range(nb_args)]
    i = command(*args, pointer = i)
