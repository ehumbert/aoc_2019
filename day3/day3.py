from collections import defaultdict

wire = 1
grid = defaultdict(list)

for line in open('input.txt'):
    directions = line.strip().split(',')

    x, y = 0, 0
    distance = 0
    for direction in directions:
        for i in range(int(direction[1:])):
            if direction[0] == 'U':
                y += 1
            elif direction[0] == 'R':
                x += 1
            elif direction[0] == 'D':
                y -= 1
            elif direction[0] == 'L':
                x -= 1

            prev = grid.get((x,y), False)
            distance += 1
            if prev:
                same_wire = False
                for p in prev:
                    if p['wire'] == wire:
                        # If we follow the instructions : If a wire visits a position on the grid multiple times, use the steps value from the first time it visits that position
                        # but that's not the case ...
                        # distance = p['distance']
                        same_wire = True
                        break
                if same_wire:
                    continue

            grid[(x,y)].append({'wire': wire, 'distance': distance})

    wire += 1

intersections = list(filter(lambda item: len(item[1]) > 1, grid.items()))
res = min(intersections, key=lambda item: sum(map(abs, item[0])))[0]

print(f"Part 1 : {sum(map(abs, res))}")
print(f"Part 2 : {min([sum(map(lambda infos: abs(infos['distance']), intersection[1])) for intersection in intersections])}")
